const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TransactionSchema = new Schema ({

	userId : {
		type : String,
		required : true
	},

	dateCreated : {
		type : Date,
		default : Date.now
	},

	transactionCode : {
		type : String,
		required : true,
		unique : true		
	},

	status : {
		type : String,
		default : "Pending"
	},

	paymentMode : {
		type : String,
		default : "Over the counter"
	},

	total : {
		type : Number,
		required : true
	},

	advisers : [
		{
			adviserId : String,
			name: String, 
			quantity : Number,
			price: Number, 
			subtotal : Number,
			paymentMode: String,
			days: Object  
		}
	] 

});

const Transaction = mongoose.model('Transaction', TransactionSchema);
module.exports = Transaction;